import requests
from bs4 import BeautifulSoup

def main():
    def scraping_cameras_maxelectro():
        URL = 'https://maxelektro.pl/wyszukiwarka/szukaj.html?query=kamera' #maxelectro.pl
        page = requests.get(URL)
        print(page.url)
        soup = BeautifulSoup(page.content, 'html.parser')
        results = soup.find(class_='tiles')
        items = results.find_all('div', class_='content')
        f.write("Kamery ze sklepu maxelectro.pl\n\n\n")
        #print(items)

        for item in items:
            name = item.find('h3', class_='title')
            price = item.find('div', class_='price')
            if None in (name, price):
                continue
            price = price.text
            price = price.replace(",", ".")
            price = price.replace("\r", "")
            price = price.replace("\n", "")
            price = price.replace("zł", "")
            name = name.text
            name = name.replace("Kamera", "")
            name = name.replace("internetowa", "")
            name = name.replace("sportowa", "")
            name = name.strip()
            cameras_maxelectro.append([name, float(price)])
            f.write(name+"\n")
            f.write(price+"\n\n")
            print(name)
            print(price)
            print()


    def scraping_cameras_skapiec():
        f.write("\n\nKamery ze sklepu maxelectro.pl\n\n\n")
        for i in range(10):
            try:
                URL = 'https://www.skapiec.pl/cat/18/page/{}'.format(i+1) #skapiec.pl
                page = requests.get(URL)
                if i != 0 and page.url =='https://www.skapiec.pl/cat/18-kamery-cyfrowe.html':
                    break
                print(page.url)
                soup = BeautifulSoup(page.content, 'html.parser')
                results = soup.find(class_='products-list-wrapper')
                items = results.find_all('div', class_='box-row')
                #print(items)

                for item in items:
                    name = item.find('h2', class_='title')
                    price = item.find('strong', class_='price')
                    if None in (name, price):
                        continue
                    price = price.text
                    price = price.replace(" ", "")
                    price = price.replace(",", ".")
                    price = price.replace("od", "")
                    price = price.replace("zł", "")
                    name = name.text
                    name = name.replace("kamera", "")
                    name = name.replace("samochodowa", "")
                    name = name.replace("cyfrowa", "")
                    name = name.strip()
                    cameras_skapiec.append([name, float(price)])
                    f.write(name + "\n")
                    f.write(price + "\n\n")
                    print(name)
                    print(price)
                    print()
            except IndexError as e:
                print(e)

    def compare_price(first, second):
        for i in first:
            found_same_cameras = False
            for j in second:
                if i[0] == j[0]:
                    found_same_cameras = True
                    if i[1] > j[1]:
                        difference = i[1] - j[1]
                        print("Kamera: {}, jest tańsza w sklepie skąpiec.pl o {} zł, i kosztuje {} zł".format(i[0], difference, j[1]))
                    elif i[1] < j[1]:
                        difference = j[1] - i[1]
                        print("Kamera: {}, jest tańsza w sklepie skąpiec.pl o {} zł, i kosztuje {} zł".format(i[0], difference, i[1]))
                    else:
                        print("Kamera: {}, kosztuje tyle samo w obu sklepach: {} zł".format(i[0], i[1]))
        if not found_same_cameras:
            print("\n\nNie znaleziono identycznych kamer")
        pass
    cameras_maxelectro = []
    cameras_skapiec = []
    f = open("Kamery.txt", "w")
    scraping_cameras_maxelectro()
    scraping_cameras_skapiec()
    print(cameras_maxelectro)
    print(cameras_skapiec)
    f.closed

    compare_price(cameras_maxelectro, cameras_skapiec)

    input("\n\nAby zapisać nazwy kamer z cenami do pliku, nacisnij enter> ")

main()