from pynput.mouse import Button, Controller
from random import randint, random
from time import sleep
mouse = Controller()

def goToOre():
	sleep(randint(1,3)+random())
	mouse.position = (1866, 539)
	sleep(random())
	mouse.click(Button.left, 1)
	sleep(randint(40,43)+random())

def mining():
	for i in range(10):
		sleep(1+random())
		for i in range(randint(10, 25)):
			#mining
			mouse.position = (990, 600)
			mouse.click(Button.left, 1)
			sleep(randint(3,7)+random())
		#filling
		sleep(randint(0,2)+random())
		mouse.position = (1890, 890)
		sleep(random())
		mouse.click(Button.left, 1)
		sleep(randint(2,3)+random())

def goToForge():
	sleep(1+random())
	#1/2 roadToForge
	mouse.position = (1828, 73)
	sleep(random())
	mouse.click(Button.left, 1)
	sleep(35)
	#goToForge
	mouse.position = (1651, 385)
	sleep(random())
	mouse.click(Button.left, 1)
	sleep(20)
	#click onForge
	mouse.position = (920, 404)
	sleep(random())
	mouse.click(Button.left, 1)
	sleep(4+random())
	#deposite
	mouse.position = (628, 592)
	sleep(random())
	mouse.click(Button.left, 1)
	sleep(2+random())

while True:
	goToOre()
	mining()
	goToForge()
