from pynput.keyboard import Key, Controller
from time import sleep
from random import random, randint

keyboard = Controller()

# Press and release space
while True:
	sleep(randint(1, 5) + random())
	cycle = randint(50, 300)
	for i in range(cycle):
		sleep(randint(0,1)+random())
		attacking = randint(3, 6)
		for j in range(attacking):
			sleep(0.1 + random())
			keyboard.press('2')
			keyboard.release('2')
		sleep(0.1 + random())
		keyboard.press('z')
		sleep(0.1 + random())
		keyboard.release('z')
