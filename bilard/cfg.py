import pygame
import os

pygame.init()
screen_width = 412
screen_height = 770
screen = pygame.display.set_mode((screen_width, screen_height))
os.environ['SDL_VIDEO_WINDOW_POS'] = '{}, {}'.format(0, 0)

speed_decrease = 0.99
