import pygame
import math
from cfg import screen, speed_decrease, screen_width, screen_height

class Ball:
    def __init__(self):
        self.x = 100
        self.y = 100
        self.radius = 12
        self.color = (255, 100, 100)
        self.coefs = [0, 0]
        self.screen = screen
        self.speed = 0

    def draw(self):
        pygame.draw.circle(self.screen, self.color, (self.x, self.y), self.radius)

    # calculates vector
    # have to be recalculate after hiting the obstacle
    def hit(self, mouse_x, mouse_y):
        delta_x = self.x - mouse_x
        delta_y = self.y - mouse_y
        x_coef = delta_x / (abs(delta_x) + abs(delta_y))
        y_coef = delta_y / (abs(delta_x) + abs(delta_y))
        self.coefs = [x_coef, y_coef]

    def move(self):
        if self.speed > 0:
            self.x += self.coefs[0] * self.speed
            self.y += self.coefs[1] * self.speed
            self.speed *= speed_decrease

    #slow down after hit wall
    def wall_collisions_check(self):
        if self.x - self.radius < 0:
            self.x = 0 + self.radius
            self.coefs[0] *= -1
        elif self.x + self.radius > screen_width:
            self.x = screen_width - self.radius
            self.coefs[0] *= -1
        elif self.y - self.radius < 0:
            self.y = 0 + self.radius
            self.coefs[1] *= -1
        elif self.y + self.radius > screen_height:
            self.y = screen_height - self.radius
            self.coefs[1] *= -1

    def ball_collision_check(self, other_ball):
        dx = abs(self.x - other_ball.x)
        dy = abs(self.y - other_ball.y)
        d = math.sqrt(dx**2 + dy**2)
        if d < self.radius:
            other_ball.coefs = self.coefs
            other_ball.speed = self.speed
class Bill(Ball):
    def __init__(self):
        Ball.__init__(self)
        self.x = 300
        self.y = 300
        self.radius = 12
        self.color = (255, 255, 255)
        self.coefs = [0, 0]


    def aim_line(self):
        if self.speed <= 0:
            mouse = pygame.mouse.get_pos()
            pygame.draw.line(screen, (255, 255, 255), (mouse[0], mouse[1]), (self.x, self.y))

