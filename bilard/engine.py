import pygame
from cfg import screen, screen_width, screen_height
from datetime import datetime
import time

class Engine:
    def __init__(self):
        self.prev_frame = 0
        self.fps = 100
        self.frame_counter = 0

    def update(self):
        pygame.display.update()

    def time(self):
        time.sleep(0.000001)
        now = int(datetime.now().strftime("%Y%m%d%H%M%S%f"))
        frame_durr = int(1000000/self.fps)
        self.frame_counter += 1
        print(int((now - self.prev_frame)*100/(frame_durr)), "%")
        while now - self.prev_frame < frame_durr:
            time.sleep(0.001)
            now = int(datetime.now().strftime("%Y%m%d%H%M%S%f"))
        self.prev_frame = now

