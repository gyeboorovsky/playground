import pygame
from pygame.locals import *

def handler(bill):
    run = True

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            run = False
        if event.type == KEYDOWN:
            pass
        if event.type == MOUSEBUTTONDOWN:
            bill.speed += 20
            hits = pygame.mouse.get_pos()
            bill.hit(hits[0], hits[1])

    return run