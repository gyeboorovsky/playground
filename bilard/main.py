import pygame
from cfg import screen
import events
from engine import Engine
from BallClass import Ball, Bill

def main():
    bill = Bill()
    ball1 = Ball()
    game = Engine()
    run = True
    while run:
        game.time()
        run = events.handler(bill)
        screen.fill((0, 0, 0))
        #
        bill.move()
        bill.wall_collisions_check()
        bill.ball_collision_check(ball1)
        bill.draw()
        bill.aim_line()
        #
        ball1.move()
        ball1.wall_collisions_check()
        ball1.draw()
        #
        game.update()



if __name__ == '__main__':
    main()

