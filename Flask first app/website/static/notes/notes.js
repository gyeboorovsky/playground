l=1
addTask = () => {
    if (l <= 8){
        var createButton = document.createElement('button');
        var createBasketButton = document.createElement('button'); 
        var createTextArea = document.createElement('textarea');
        var createInput = document.createElement('input');
        var createP = document.createElement('p');
        //button
        tasks.appendChild(createButton);
        var button = tasks.lastChild;
        button.setAttribute("class","task");
        button.setAttribute("id",`task${l}`);
        button.setAttribute("onclick","taskClick(this.id)")
        //checkbox
        button.appendChild(createInput);
        button.lastChild.setAttribute("class","checkbox")
        button.lastChild.setAttribute("type","checkbox");
        //p
        button.appendChild(createP);
        button.lastChild.setAttribute("id",`p${l}`);
        var text = document.getElementById("taskInput").value;
        button.lastChild.innerText = text;
        //basket
        tasks.appendChild(createBasketButton);
        tasks.lastChild.setAttribute("class","basket");
        tasks.lastChild.setAttribute("id",`basket${l}`);
        tasks.lastChild.setAttribute("onclick","delet(this.id)");
        //description
        description.appendChild(createTextArea);
        description.lastChild.setAttribute("class","description");
        description.lastChild.setAttribute("id",`description${l}`);
        description.lastChild.setAttribute("display","none");
        l += 1;
    };
};
taskClick = (clickedID) =>{
    allDesc = document.getElementById("description");
    count = allDesc.childElementCount;
    for (i = 1; i <= count; i++){
        desc =  document.getElementById(`description${i}`);
        style = window.getComputedStyle(desc);
        vis = style.getPropertyValue("visibility");
    
        if (vis == "visible"){
            desc.style.visibility = "hidden";
        };
    };
    id = clickedID[clickedID.length-1];
    desc = document.getElementById(`description${id}`);
    desc.style.visibility = "visible";
};
delet = (id) =>{
    //removing basket button
    var element = document.getElementById(id);
    tasks.removeChild(element);
        //numer of basket id
    num = id[id.length-1];
    //removing task button
    var task = document.getElementById(`task${num}`);
    tasks.removeChild(task);
    //removing description
    var desc = document.getElementById(`description${num}`);
    description.removeChild(desc);
};


