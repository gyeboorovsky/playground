﻿using System;
using System.Collections.Generic;
using System.Windows.Controls;
using System.Windows;
using System.Globalization;

namespace intel8086
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        string[] dataSegment = new string[64 * 1024];
        Stack<string> stack = new Stack<string>();

        private List<RadioButton> registersRadioButtons = new List<RadioButton>();
        public MainWindow()
        {
            InitializeComponent();
            LinkRadioButtonsWithHalfRegs();
        }
        private void MovXchgHandler(object sender, RoutedEventArgs e)
        {
            var button = (FrameworkElement)sender;
            List<string> checkedRadioButtons = CheckCheckedRegisters();
            List<TextBox> checkedRegisters = FindRegistersCorrespondingToRadiobuttons(checkedRadioButtons);
            TextBox firstRegister = checkedRegisters[0];
            TextBox secondRegister = checkedRegisters[1];


            switch (button.Name)
            {
                case "moveButton":
                    secondRegister.Text = firstRegister.Text;
                    break;
                case "xchgButton":
                    string key = firstRegister.Text;
                    firstRegister.Text = secondRegister.Text;
                    secondRegister.Text = key;
                    break;
            }
            //MergeRegs();
        }
        private void ResetRegisterSettings(object sender, RoutedEventArgs e)
        {
            axblock.Text = "0000";
            bxblock.Text = "0000";
            cxblock.Text = "0000";
            dxblock.Text = "0000";
            //ah.Text = "00";
            //al.Text = "00";
            //bh.Text = "00";
            //bl.Text = "00";
            //ch.Text = "00";
            //cl.Text = "00";
            //dh.Text = "00";
            //dl.Text = "00";
            //MergeRegs();
        }
        private void ResetMemorySettings(object sender, RoutedEventArgs e)
        {
            sibox.Text = "0000";
            dibox.Text = "0000";
            bpbox.Text = "0000";
            spbox.Text = "0000";
            disp.Text = "0000";
        }
        private void Random(object sender, RoutedEventArgs e)
        {

            string symbols = "0123456789ABCDEF";
            string randomValue = "";
            List<TextBox> registers = new List<TextBox>() { axblock, bxblock, cxblock, dxblock };

            foreach (var item in registers)
            {
                Random rnd = new Random();
                for (int i = 0; i < 4; i++)
                {
                    randomValue += symbols[rnd.Next(0, 15)];
                }
                item.Text = randomValue;
                randomValue = "";
            }
        }
        private void Validation()
        {

        }
        private List<TextBox> FindRegistersCorrespondingToRadiobuttons(List<string> checkedRadioButtons)
        {
            List<TextBox> checkedRegisters = new List<TextBox>();
            TextBox FirstCheckedRegister = new TextBox();
            TextBox SecondCheckedRegister = new TextBox();
            switch (checkedRadioButtons[0])
            {
                case "AX":
                    FirstCheckedRegister = axblock;
                    break;
                case "BX":
                    FirstCheckedRegister = bxblock;
                    break;
                case "CX":
                    FirstCheckedRegister = cxblock;
                    break;
                case "DX":
                    FirstCheckedRegister = dxblock;
                    break;

            }
            switch (checkedRadioButtons[1])
            {
                case "AX":
                    SecondCheckedRegister = axblock;
                    break;
                case "BX":
                    SecondCheckedRegister = bxblock;
                    break;
                case "CX":
                    SecondCheckedRegister = cxblock;
                    break;
                case "DX":
                    SecondCheckedRegister = dxblock;
                    break;

            }
            checkedRegisters.Add(FirstCheckedRegister);
            checkedRegisters.Add(SecondCheckedRegister);
            return checkedRegisters;
        }
        private List<string> CheckCheckedRegisters()
        {
            List<string> checkedRegisters = new List<string>();
            foreach (var r in registersRadioButtons)
            {
                if ((bool)r.IsChecked)
                {
                    checkedRegisters.Add(r.Content.ToString());
                }
            }
            return checkedRegisters;
        }
        private void LinkRadioButtonsWithHalfRegs()
        {
            registersRadioButtons.Add(a1);
            registersRadioButtons.Add(b1);
            registersRadioButtons.Add(c1);
            registersRadioButtons.Add(d1);
            registersRadioButtons.Add(a2);
            registersRadioButtons.Add(b2);
            registersRadioButtons.Add(c2);
            registersRadioButtons.Add(d2);
        }

        //private void MergeRegs()
        //{
        //    axblock.Text = ah.Text + al.Text;
        //    bxblock.Text = bh.Text + bl.Text;
        //    cxblock.Text = ch.Text + cl.Text;
        //    dxblock.Text = dh.Text + dl.Text;
        //    //ah.Text = axblock.Text.Substring(0, 2);
        //    //al.Text = axblock.Text.Substring(2, 2);
        //    //bh.Text
        //    //bl.Text
        //    //ch.Text
        //    //cl.Text
        //    //dh.Text
        //    //dl.Text
        //}

        // >3.5
        private void MOVmemory(object sender, RoutedEventArgs e)
        {
            if ((bool)index.IsChecked)
            {
                if ((bool)isi.IsChecked)
                {
                    if ((bool)regToMemory.IsChecked)
                    {
                        regToMemoryIsi();
                    }
                    else if ((bool)memoryToReg.IsChecked)
                    {
                        memoryToRegIsi();
                    }
                }
                else if ((bool)idi.IsChecked)
                {
                    if ((bool)regToMemory.IsChecked)
                    {
                        regToMemoryIdi();
                    }
                    else if ((bool)memoryToReg.IsChecked)
                    {
                        memoryToRegIdi();
                    }
                }
            }
            else if ((bool)_base.IsChecked)
            {
                if ((bool)bbx.IsChecked)
                {
                    if ((bool)regToMemory.IsChecked)
                    {
                        regToMemoryBbx();
                    }
                    else if ((bool)memoryToReg.IsChecked)
                    {
                        memoryToRegBbx();
                    }
                }
                else if ((bool)bbp.IsChecked)
                {
                    if ((bool)regToMemory.IsChecked)
                    {
                        regToMemoryBbp();
                    }
                    else if ((bool)memoryToReg.IsChecked)
                    {
                        memoryToRegBbp();
                    }
                }
            }
            else if ((bool)indexBase.IsChecked)
            {
                if ((bool)sibx.IsChecked)
                {
                    if ((bool)regToMemory.IsChecked)
                    {
                        regToMemorySibx();
                    }
                    else if ((bool)memoryToReg.IsChecked)
                    {
                        memoryToRegSibx();
                    }
                }
                else if ((bool)dibx.IsChecked)
                {
                    if ((bool)regToMemory.IsChecked)
                    {
                        regToMemoryDibx();
                    }
                    else if ((bool)memoryToReg.IsChecked)
                    {
                        memoryToRegDibx();
                    }
                }
                else if ((bool)sibp.IsChecked)
                {
                    if ((bool)regToMemory.IsChecked)
                    {
                        regToMemorySibp();
                    }
                    else if ((bool)memoryToReg.IsChecked)
                    {
                        memoryToRegSibp();
                    }
                }
                else if ((bool)dibp.IsChecked)
                {
                    if ((bool)regToMemory.IsChecked)
                    {
                        regToMemoryDibp();
                    }
                    else if ((bool)memoryToReg.IsChecked)
                    {
                        memoryToRegDibp();
                    }
                }
            }
        }

        private void memoryToRegDibp()
        {
            int num1 = int.Parse(dibox.Text, NumberStyles.HexNumber);
            int num2 = int.Parse(disp.Text, NumberStyles.HexNumber);
            int num3 = int.Parse(bpbox.Text, NumberStyles.HexNumber);
            string current = dataSegment[(num1 + num2 + num3)];
            current += dataSegment[(num1 + num2 + num3)+1];
            CheckedRegContent().Text = current;
        }

        private void regToMemoryDibp()
        {
            int num1 = int.Parse(dibox.Text, NumberStyles.HexNumber);
            int num2 = int.Parse(disp.Text, NumberStyles.HexNumber);
            int num3 = int.Parse(bpbox.Text, NumberStyles.HexNumber);
            dataSegment[(num1 + num2 + num3)] = CheckedRegContent().Text.Substring(0, 2);
            dataSegment[(num1 + num2 + num3)+1] = CheckedRegContent().Text.Substring(2, 2);
        }

        private void memoryToRegSibp()
        {
            int num1 = int.Parse(sibox.Text, NumberStyles.HexNumber);
            int num2 = int.Parse(disp.Text, NumberStyles.HexNumber);
            int num3 = int.Parse(bpbox.Text, NumberStyles.HexNumber);
            string current = dataSegment[(num1 + num2 + num3)];
            current += dataSegment[(num1 + num2 + num3) + 1];
            CheckedRegContent().Text = current;
        }

        private void regToMemorySibp()
        {
            int num1 = int.Parse(sibox.Text, NumberStyles.HexNumber);
            int num2 = int.Parse(disp.Text, NumberStyles.HexNumber);
            int num3 = int.Parse(bpbox.Text, NumberStyles.HexNumber);
            dataSegment[(num1 + num2 + num3)] = CheckedRegContent().Text.Substring(0, 2);
            dataSegment[(num1 + num2 + num3) + 1] = CheckedRegContent().Text.Substring(2, 2);
        }

        private void memoryToRegDibx()
        {
            int num1 = int.Parse(dibox.Text, NumberStyles.HexNumber);
            int num2 = int.Parse(disp.Text, NumberStyles.HexNumber);
            int num3 = int.Parse(bxblock.Text, NumberStyles.HexNumber);
            string current = dataSegment[(num1 + num2 + num3)];
            current += dataSegment[(num1 + num2 + num3) + 1];
            CheckedRegContent().Text = current;
        }

        private void regToMemoryDibx()
        {
            int num1 = int.Parse(dibox.Text, NumberStyles.HexNumber);
            int num2 = int.Parse(disp.Text, NumberStyles.HexNumber);
            int num3 = int.Parse(bxblock.Text, NumberStyles.HexNumber);
            dataSegment[(num1 + num2 + num3)] = CheckedRegContent().Text.Substring(0, 2);
            dataSegment[(num1 + num2 + num3) + 1] = CheckedRegContent().Text.Substring(2, 2);
        }

        private void memoryToRegSibx()
        {
            int num1 = int.Parse(sibox.Text, NumberStyles.HexNumber);
            int num2 = int.Parse(disp.Text, NumberStyles.HexNumber);
            int num3 = int.Parse(bxblock.Text, NumberStyles.HexNumber);
            string current = dataSegment[(num1 + num2 + num3)];
            current += dataSegment[(num1 + num2 + num3) + 1];
            CheckedRegContent().Text = current;
        }

        private void regToMemorySibx()
        {
            int num1 = int.Parse(sibox.Text, NumberStyles.HexNumber);
            int num2 = int.Parse(disp.Text, NumberStyles.HexNumber);
            int num3 = int.Parse(bxblock.Text, NumberStyles.HexNumber);
            dataSegment[(num1 + num2 + num3)] = CheckedRegContent().Text.Substring(0, 2);
            dataSegment[(num1 + num2 + num3) + 1] = CheckedRegContent().Text.Substring(2, 2);
        }

        private void memoryToRegBbp()
        {
            int num1 = int.Parse(bpbox.Text, NumberStyles.HexNumber);
            int num2 = int.Parse(disp.Text, NumberStyles.HexNumber);
            string current = dataSegment[(num1 + num2)];
            current += dataSegment[(num1 + num2) + 1];
            CheckedRegContent().Text = current;
        }

        private void regToMemoryBbp()
        {
            int num1 = int.Parse(bpbox.Text, NumberStyles.HexNumber);
            int num2 = int.Parse(disp.Text, NumberStyles.HexNumber);
            dataSegment[(num1 + num2)] = CheckedRegContent().Text.Substring(0, 2);
            dataSegment[(num1 + num2) + 1] = CheckedRegContent().Text.Substring(2, 2);
        }

        private void memoryToRegBbx()
        {
            int num1 = int.Parse(bxblock.Text, NumberStyles.HexNumber);
            int num2 = int.Parse(disp.Text, NumberStyles.HexNumber);
            string current = dataSegment[(num1 + num2)];
            current += dataSegment[(num1 + num2) + 1];
            CheckedRegContent().Text = current;
        }

        private void regToMemoryBbx()
        {
            int num1 = int.Parse(bxblock.Text, NumberStyles.HexNumber);
            int num2 = int.Parse(disp.Text, NumberStyles.HexNumber);
            dataSegment[(num1 + num2)] = CheckedRegContent().Text.Substring(0, 2);
            dataSegment[(num1 + num2) + 1] = CheckedRegContent().Text.Substring(2, 2);
        }

        private void memoryToRegIdi()
        {
            int num1 = int.Parse(dibox.Text, NumberStyles.HexNumber);
            int num2 = int.Parse(disp.Text, NumberStyles.HexNumber);
            string current = dataSegment[(num1 + num2)];
            current += dataSegment[(num1 + num2) + 1];
            CheckedRegContent().Text = current;
        }

        private void regToMemoryIdi()
        {
            int num1 = int.Parse(dibox.Text, NumberStyles.HexNumber);
            int num2 = int.Parse(disp.Text, NumberStyles.HexNumber);
            dataSegment[(num1 + num2)] = CheckedRegContent().Text.Substring(0, 2);
            dataSegment[(num1 + num2) + 1] = CheckedRegContent().Text.Substring(2, 2);
        }

        private void memoryToRegIsi()
        {
            int num1 = int.Parse(sibox.Text, NumberStyles.HexNumber);
            int num2 = int.Parse(disp.Text, NumberStyles.HexNumber);
            string current = dataSegment[(num1 + num2)];
            current += dataSegment[(num1 + num2) + 1];
            CheckedRegContent().Text = current;
        }

        private void regToMemoryIsi()
        {
            int num1 = int.Parse(sibox.Text, NumberStyles.HexNumber);
            int num2 = int.Parse(disp.Text, NumberStyles.HexNumber);
            dataSegment[(num1 + num2)] = CheckedRegContent().Text.Substring(0, 2);
            dataSegment[(num1 + num2) + 1] = CheckedRegContent().Text.Substring(2, 2);
        }

        private TextBox CheckedRegContent()
        {
            if ((bool)pax.IsChecked)
            {
                return axblock;
            }
            else if ((bool)pbx.IsChecked)
            {
                return bxblock;
            }
            else if ((bool)pcx.IsChecked)
            {
                return cxblock;
            }
            else if ((bool)pdx.IsChecked)
            {
                return dxblock;
            }
            else
            {
                return new TextBox();
            }
        }

        private void XCHGmemory(object sender, RoutedEventArgs e)
        {
            if ((bool)index.IsChecked)
            {
                if ((bool)isi.IsChecked)
                {
                    xchgIsi();
                }
                else if ((bool)idi.IsChecked)
                {
                    xchgIdi();
                }
            }
            else if ((bool)_base.IsChecked)
            {
                if ((bool)bbx.IsChecked)
                {
                    xchgBbx();
                }
                else if ((bool)bbp.IsChecked)
                {
                    xchgBbp();
                }
            }
            else if ((bool)indexBase.IsChecked)
            {
                if ((bool)sibx.IsChecked)
                {
                    xchgSibx();
                }
                else if ((bool)dibx.IsChecked)
                {
                    xchgDibx();
                }
                else if ((bool)sibp.IsChecked)
                {
                    xchgSibp();
                }
                else if ((bool)dibp.IsChecked)
                {
                    memoryRegDibp();
                }
            }
        }


        private void xchgIsi()
        {
            int num1 = int.Parse(sibox.Text, NumberStyles.HexNumber);
            int num2 = int.Parse(disp.Text, NumberStyles.HexNumber);
            string regContent = CheckedRegContent().Text;
            CheckedRegContent().Text = dataSegment[(num1 + num2)];
            CheckedRegContent().Text += dataSegment[(num1 + num2) + 1];
            dataSegment[(num1 + num2)] = regContent.Substring(0, 2);
            dataSegment[(num1 + num2) + 1] = regContent.Substring(2, 2);
        }

        private void xchgIdi()
        {
            int num1 = int.Parse(dibox.Text, NumberStyles.HexNumber);
            int num2 = int.Parse(disp.Text, NumberStyles.HexNumber);
            string regContent = CheckedRegContent().Text;
            CheckedRegContent().Text = dataSegment[(num1 + num2)];
            CheckedRegContent().Text += dataSegment[(num1 + num2) + 1];
            dataSegment[(num1 + num2)] = regContent.Substring(0, 2);
            dataSegment[(num1 + num2) + 1] = regContent.Substring(2, 2);
        }

        private void xchgBbx()
        {
            int num1 = int.Parse(bxblock.Text, NumberStyles.HexNumber);
            int num2 = int.Parse(disp.Text, NumberStyles.HexNumber);
            string regContent = CheckedRegContent().Text;
            CheckedRegContent().Text = dataSegment[(num1 + num2)];
            CheckedRegContent().Text += dataSegment[(num1 + num2) + 1];
            dataSegment[(num1 + num2)] = regContent.Substring(0, 2);
            dataSegment[(num1 + num2) + 1] = regContent.Substring(2, 2);
        }

        private void xchgBbp()
        {
            int num1 = int.Parse(bpbox.Text, NumberStyles.HexNumber);
            int num2 = int.Parse(disp.Text, NumberStyles.HexNumber);
            string regContent = CheckedRegContent().Text;
            CheckedRegContent().Text = dataSegment[(num1 + num2)];
            CheckedRegContent().Text += dataSegment[(num1 + num2) + 1];
            dataSegment[(num1 + num2)] = regContent.Substring(0, 2);
            dataSegment[(num1 + num2) + 1] = regContent.Substring(2, 2);
        }

        private void xchgSibx()
        {
            int num1 = int.Parse(sibox.Text, NumberStyles.HexNumber);
            int num2 = int.Parse(disp.Text, NumberStyles.HexNumber);
            int num3 = int.Parse(bxblock.Text, NumberStyles.HexNumber);
            string regContent = CheckedRegContent().Text;
            CheckedRegContent().Text = dataSegment[(num1 + num2 + num3)];
            CheckedRegContent().Text += dataSegment[(num1 + num2 + num3) + 1];
            dataSegment[(num1 + num2 + num3)] = regContent.Substring(0, 2);
            dataSegment[(num1 + num2 + num3) + 1] = regContent.Substring(2, 2);
        }

        private void xchgDibx()
        {
            int num1 = int.Parse(dibox.Text, NumberStyles.HexNumber);
            int num2 = int.Parse(disp.Text, NumberStyles.HexNumber);
            int num3 = int.Parse(bxblock.Text, NumberStyles.HexNumber);
            string regContent = CheckedRegContent().Text;
            CheckedRegContent().Text = dataSegment[(num1 + num2 + num3)];
            CheckedRegContent().Text += dataSegment[(num1 + num2 + num3) + 1];
            dataSegment[(num1 + num2 + num3)] = regContent.Substring(0, 2);
            dataSegment[(num1 + num2 + num3) + 1] = regContent.Substring(2, 2);
        }

        private void xchgSibp()
        {
            int num1 = int.Parse(sibox.Text, NumberStyles.HexNumber);
            int num2 = int.Parse(disp.Text, NumberStyles.HexNumber);
            int num3 = int.Parse(bpbox.Text, NumberStyles.HexNumber);
            string regContent = CheckedRegContent().Text;
            CheckedRegContent().Text = dataSegment[(num1 + num2 + num3)];
            CheckedRegContent().Text += dataSegment[(num1 + num2 + num3) + 1];
            dataSegment[(num1 + num2 + num3)] = regContent.Substring(0, 2);
            dataSegment[(num1 + num2 + num3) + 1] = regContent.Substring(2, 2);
        }

        private void memoryRegDibp()
        {
            int num1 = int.Parse(dibox.Text, NumberStyles.HexNumber);
            int num2 = int.Parse(disp.Text, NumberStyles.HexNumber);
            int num3 = int.Parse(bpbox.Text, NumberStyles.HexNumber);
            string regContent = CheckedRegContent().Text;
            CheckedRegContent().Text = dataSegment[(num1 + num2 + num3)];
            CheckedRegContent().Text += dataSegment[(num1 + num2 + num3) + 1];
            dataSegment[(num1 + num2 + num3)] = regContent.Substring(0, 2);
            dataSegment[(num1 + num2 + num3) + 1] = regContent.Substring(2, 2);
        }

        private void push(object sender, RoutedEventArgs e)
        {
            if (stack.Count >= 64*1024)
            {
                spbox.Text = "0000";
            }
            else
            {
                stack.Push(CheckedRegContent().Text.Substring(0, 2));
                stack.Push(CheckedRegContent().Text.Substring(2, 2));

                int spDec = int.Parse(spbox.Text, NumberStyles.HexNumber);
                spDec += 2;
                spbox.Text = spDec.ToString("X");
            }
        }

        private void pop(object sender, RoutedEventArgs e)
        {
            string l = stack.Pop();
            string h = stack.Pop();
            CheckedRegContent().Text = h + l;

            int spDec = int.Parse(spbox.Text, NumberStyles.HexNumber);
            spDec -= 2;
            spbox.Text = spDec.ToString("X");
        }
    }
}