from functools import wraps
import time

def my_decorator(func):
    @wraps(func)
    def wrapped(*args, **kwargs):
        start = time.time()
        func(*args, **kwargs)
        end = time.time()
        print(f"Time Elapsed: {end-start}")

    return wrapped

@my_decorator
def sum_of_squares(a, b):
    s = lambda c, d: print(c ** d)
    s(a, b)
