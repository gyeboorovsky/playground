﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using WebDhiAPI.DataAccess;
using WebDhiAPI.Models;

namespace PostgresCRUD.Controllers
{
    [Route("api/[controller]")]
    public class PersonController : Controller
    {
        private readonly PostgreSqlContext _context;
        public PersonController(PostgreSqlContext context)
        {
            _context = context;
        }

        [HttpGet]
        public IEnumerable<Person> GetAll()
        {
            return _context.Persons.ToList();
        }

        [HttpPost]
        public ActionResult<Person> Create(Person person)
        {
            var exist = _context.Persons.FirstOrDefault();
            if (exist == null)
            {
                person.Id = 1;
            }
            else
            {
                person.Id = _context.Persons.Max(p => p.Id) + 1;
            }
            
            _context.Persons.Add(person);
            _context.SaveChanges();
            return person;
        }

        [HttpPut]
        public IActionResult Edit([FromBody] Person person)
        {
            if (ModelState.IsValid)
            {
                _context.Persons.Update(person);
                _context.SaveChanges();
                return Ok();
            }
            return BadRequest();
        }

        [HttpDelete("{id}")]
        public ActionResult<Person> DeletePerson(int id)
        {
            var person = _context.Persons.Find(id);
            if (person == null)
            {
                return NotFound();
            }
            var entity = _context.Persons.FirstOrDefault(t => t.Id == id);
            _context.Persons.Remove(entity);
            _context.SaveChanges();
            return Ok();
        }
    }
}
