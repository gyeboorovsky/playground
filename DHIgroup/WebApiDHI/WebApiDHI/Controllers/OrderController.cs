﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebDhiAPI.DataAccess;
using WebDhiAPI.Models;

namespace PostgresCRUD.Controllers
{
    [Route("api/[controller]")]
    public class OrderController : Controller
    {
        private readonly PostgreSqlContext _context;
        public OrderController(PostgreSqlContext context)
        {
            _context = context;
        }

        [HttpGet("{id}")]
        public IEnumerable<Vehicle> GetAllVehicleBougthByPersonId(int id)
        {
            var personOrders = _context.Orders.Where(o => o.PersonId == id).ToList();
            List<Vehicle> vehicles = new List<Vehicle>();
            for (int i = 0; i < personOrders.Count(); i++)
            {
                vehicles.Add(
                    _context.Vehicles
                    .FirstOrDefault(
                        v => v.Id == personOrders[i].VehicleId));
            }
            return vehicles;
        }

        [HttpPost]
        public ActionResult<Order> Create(Order order)
        {
            var exist = _context.Orders.FirstOrDefault();
            if (exist == null)
            {
                order.Id = 1;
            }
            else
            {
                order.Id = _context.Orders.Max(o => o.Id) + 1;
            }

            var vehicle = _context.Vehicles.FirstOrDefault(i => i.Id == order.VehicleId);
            vehicle.Ordered = true;
            _context.Vehicles.Update(vehicle);

            _context.Orders.Add(order);
            _context.SaveChanges();
            return order;
        }
    }
}
