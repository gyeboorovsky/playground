﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebDhiAPI.DataAccess;
using WebDhiAPI.Models;

namespace WebApiDHI.Controllers
{
    [Route("api/[controller]")]
    public class VehicleController : Controller
    {
        private readonly PostgreSqlContext _context;
        public VehicleController(PostgreSqlContext context)
        {
            _context = context;
        }

        [HttpGet]
        public IEnumerable<Vehicle> GetAll()
        {
            return _context.Vehicles.ToList();
        }

        [HttpPost]
        public ActionResult<Vehicle> Create(Vehicle vehicle)
        {
            var exist = _context.Vehicles.FirstOrDefault();
            if (exist == null)
            {
                vehicle.Id = 1;
            }
            else
            {
                vehicle.Id = _context.Vehicles.Max(i => i.Id) + 1;
            }

            if (_context.Vehicles.Any(v => v.VinNumber == vehicle.VinNumber))
            {
                throw new Exception("There is vahicle with that serial number");
            }
            _context.Vehicles.Add(vehicle);
            _context.SaveChanges();
            return vehicle;
        }

        [HttpPut]
        public ActionResult<Vehicle> Edit(Vehicle vehicle)
        {
            _context.Vehicles.Update(vehicle);
            _context.SaveChanges();
            return vehicle;
        }

        [HttpDelete("{id}")]
        public ActionResult<Vehicle> DeleteItem(int id)
        {
            var item = _context.Vehicles.Find(id);
            if (item == null)
            {
                return NotFound();
            }
            var entity = _context.Vehicles.FirstOrDefault(t => t.Id == id);
            _context.Vehicles.Remove(entity);
            _context.SaveChanges();
            return Ok();
        }
    }
}
