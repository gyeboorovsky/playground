﻿using Microsoft.EntityFrameworkCore;
using WebDhiAPI.Models;

namespace WebDhiAPI.DataAccess
{
    public class PostgreSqlContext : DbContext
    {
        public PostgreSqlContext(DbContextOptions<PostgreSqlContext> options) : base(options)
        {
        }

        public DbSet<Person> Persons { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<Vehicle> Vehicles { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<Vehicle>()
                .Property(p => p.Ordered)
                .HasDefaultValue(false);
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
            => optionsBuilder.UseNpgsql("User ID=postgres;Password=mama3124;Host=localhost;Port=5432;Database=SampleDatabase;Pooling=true;Connection Lifetime=0;");
    }
}