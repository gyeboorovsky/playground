﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WebDhiAPI.Models
{
    public class Order
    {
        public int Id { get; set; }
        [ForeignKey("Person")]
        [Required]
        public int PersonId { get; set; }
        [ForeignKey("Item")]
        [Required]
        public int VehicleId { get; set; }
    }
}
