﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WebDhiAPI.Models
{
    public class Vehicle
    {
        public int Id { get; set; }
        [Required]
        public string Model { get; set; }
        [Required]
        public string Brand { get; set; }
        [Required]
        public int ProductionYear { get; set; }
        [Required]
        public string VinNumber { get; set; }
        [Required]
        [Column(TypeName = "money")]
        public decimal Price { get; set; }
        public bool Ordered { get; set; }
    }
}
