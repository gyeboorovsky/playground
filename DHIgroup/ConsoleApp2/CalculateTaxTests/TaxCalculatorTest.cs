﻿using CalculateTax;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;

namespace CalculateTaxTests
{
    [TestClass]
    public class TaxCalculatorTest
    {
        [TestMethod]
        public void CalculateTaxTest()
        {
            //Arrange
            int[] incomes = {
                12000,
                100000,
                200000,
                657473,
                100000000};

            Dictionary<double, double> taxTresholds = new Dictionary<double, double>();
                taxTresholds.Add(10000, 0);
                taxTresholds.Add(30000, 0.05);
                taxTresholds.Add(100000, 0.10);
                taxTresholds.Add(300000, 0.20);
                taxTresholds.Add(1000000, 0.40);
                taxTresholds.Add(10000000, 0.60);
                taxTresholds.Add(-1, 0.80);

            string filePath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            string fileWithTaxPartInPercent = "Progi podatkowe w %.txt";

            TaxCalculator taxCalculator = new(filePath, fileWithTaxPartInPercent, 100);
            taxCalculator.TaxTresholds = taxTresholds;

            int[] results = new int[incomes.Length];

            //Act
            for (int i = 0; i < incomes.Length; i++)
            {
                taxCalculator.Income = incomes[i];
                results[i] = taxCalculator.CalculateTax();

            }

            //Assert
            //Assert.AreEqual(6448000, result);
            Assert.AreEqual(100, results[0]);
            Assert.AreEqual(8000, results[1]);
            Assert.AreEqual(28000, results[2]);
            Assert.AreEqual(190989, results[3]);
            Assert.AreEqual(77728000, results[4]);

        }
    }
}