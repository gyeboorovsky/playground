using CalculateTax;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;

namespace CalculateTaxTests
{
    [TestClass]
    public class ConversionsTest
    {
        [TestMethod]
        public void ParseStringToTaxTresholdTest()
        {
            //Arrange
            List<string> mockedData = new List<string>() {
                "10000\t0%",
                "10000\t1%",
                "10000\t5%",
                "40000\t0.1",
                "40000\t0.10",
                "40000\t0.4",
                "40000\t0.40",
                "-----\t0.45"};

            List<double[]> results = new List<double[]>();

            //Act
            for (int i = 0; i < mockedData.Count; i++)
            {
                results.Add(Conversions.ParseStringToTaxTreshold(mockedData[i]));
            }

            //Assert
            Assert.AreEqual(10000, results[0][0]);
            Assert.AreEqual(40000, results[3][0]);
            Assert.AreEqual(-1, results[7][0]);

            Assert.AreEqual(0, results[0][1]);
            Assert.AreEqual(0.01, results[1][1]);
            Assert.AreEqual(0.05, results[2][1]);
            Assert.AreEqual(0.10, results[3][1]);
            Assert.AreEqual(0.10, results[4][1]);
            Assert.AreEqual(0.40, results[5][1]);
            Assert.AreEqual(0.40, results[6][1]);
            Assert.AreEqual(0.45, results[7][1]);
        }

        [TestMethod]
        public void ParsePercentToNumberTest()
        {
            //Arrange
            List<string> mockedData = new List<string>() {
                "0%",
                "1%",
                "3%",
                "4%",
                "10%",
                "99%"};

            List<double> results = new List<double>();

            //Act
            for (int i = 0; i < mockedData.Count; i++)
            {
                results.Add(Conversions.ParsePercentToNumber(mockedData[i]));
            }

            //Assert
            Assert.AreEqual(0, results[0]);
            Assert.AreEqual(0.01, results[1]);
            Assert.AreEqual(0.03, results[2]);
            Assert.AreEqual(0.04, results[3]);
            Assert.AreEqual(0.1, results[4]);
            Assert.AreEqual(0.99, results[5]);

        }

        [TestMethod]
        public void GetTaxTresholdsFromStringTest()
        {
            //Arrange
            string[] mockedData = {
                "10000\t0%",
                "20000\t1%",
                "30000\t5%",
                "40000\t0.1",
                "50000\t0.10",
                "60000\t0.4",
                "70000\t0.40",
                "-----\t0.45"
            };

            //Act
            Dictionary<double, double> result = Conversions.GetTaxTresholdsFromString(mockedData);

            double[] keys = result.Keys.ToArray();
            double[] values = result.Values.ToArray();

            //Assert
            Assert.AreEqual(10000, keys[0]);
            Assert.AreEqual(40000, keys[3]);
            Assert.AreEqual(-1, keys[7]);

            Assert.AreEqual(0, values[0]);
            Assert.AreEqual(0.01, values[1]);
            Assert.AreEqual(0.05, values[2]);
            Assert.AreEqual(0.10, values[3]);
            Assert.AreEqual(0.10, values[4]);
            Assert.AreEqual(0.40, values[5]);
            Assert.AreEqual(0.4, values[6]);
            Assert.AreEqual(0.45, values[7]);

        }
    }
}
