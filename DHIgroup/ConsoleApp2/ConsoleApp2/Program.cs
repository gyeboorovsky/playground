﻿using System;
using System.Collections.Generic;
using System.IO;

namespace CalculateTax
{
    class Program
    {
        static void Main(string[] args)
        {
            string filePath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            string fileWithTaxPartInPercent = "Progi podatkowe w %.txt";
            string fileWithTaxPartInNumber = "Progi podatkowe liczbowo 0-1.txt";

            Console.Write("What is your income? >");
            int income = int.Parse(Console.ReadLine());

            TaxCalculator taxCalculator = new(filePath, fileWithTaxPartInPercent, income);
            taxCalculator.CalculateTax();
        }
    }
}
