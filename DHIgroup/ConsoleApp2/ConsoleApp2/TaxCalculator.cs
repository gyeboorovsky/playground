﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculateTax
{
    public class TaxCalculator
    {
        public string FullPath { get; set; }
        public string[] LinesFromFile { get; set; }
        public int Income { get; set; }
        public Dictionary<double, double> TaxTresholds { get; set; }

        public TaxCalculator( string filePath, string fileName, int income)
        {
            Income = income;
            
            SetFullPath(filePath, fileName);
            
            InitializeTaxTresholds();
            
            ReadFile();
            
            SetTaxTresholds();
        }



        public int CalculateTax()
        {
            int lastTreshold = 0;
            double tax = 0;

            foreach (var item in TaxTresholds)
            {
                if (item.Key < Income && item.Key > lastTreshold)
                {
                    tax += (item.Key - lastTreshold) * item.Value;
                }
                else if (Income > lastTreshold)
                {
                    tax += (Income - lastTreshold) * item.Value;
                }
                lastTreshold = (int)item.Key;
            }

            int output = (int)tax;
            Console.WriteLine(output);
            return output;
        }

        public void SetFullPath(string filePath, string fileName) => FullPath = Path.Combine(filePath, fileName);

        public void InitializeTaxTresholds()
        {
            if (!File.Exists(FullPath))
            {
                string defaultTaxTresholds;

                if (FullPath.Contains("%"))
                {
                    defaultTaxTresholds = "" +
                        "10000\t0%\n" +
                        "30000\t10%\n" +
                        "100000\t25%\n" +
                        "-------\t40%";
                }
                else
                {
                    defaultTaxTresholds = "" +
                        "10000\t0\n" +
                        "30000\t0.1\n" +
                        "100000\t0.25\n" +
                        "-------\t0.4";
                }

                using (StreamWriter outputFile = new StreamWriter(FullPath))
                {
                    outputFile.Write(defaultTaxTresholds);
                }
            }
        }

        public void ReadFile() => LinesFromFile = File.ReadAllLines(FullPath);

        public void SetTaxTresholds() => TaxTresholds = Conversions.GetTaxTresholdsFromString(LinesFromFile);

    }
}
