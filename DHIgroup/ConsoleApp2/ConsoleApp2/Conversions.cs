﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculateTax
{
    public static class Conversions
    {
        public static Dictionary<double, double> GetTaxTresholdsFromString(string[] lines)
        {
            Dictionary<double, double> TaxTresholds = new Dictionary<double, double>();

            foreach (string line in lines)
            {
                double[] tresholds = ParseStringToTaxTreshold(line);
                TaxTresholds.Add(tresholds[0], tresholds[1]);
            }

            return TaxTresholds;
        }

        public static double[] ParseStringToTaxTreshold(string str)
        {
            double[] taxTresholds = new double[2];

            string[] stringTreshold = str.Split("\t");
            string tresholdPart = stringTreshold[0];
            string taxPart = stringTreshold[1];

            if (double.TryParse(tresholdPart, out taxTresholds[0])) { }
            else
            {
                taxTresholds[0] = -1;
            }

            if (taxPart.Contains("%"))
            {
                taxTresholds[1] = ParsePercentToNumber(taxPart);
            }
            else
            {
                taxTresholds[1] = double.Parse(taxPart);
            }

            return taxTresholds;
        }

        public static double ParsePercentToNumber(string number)
        {
            number = number.Replace("%", "");

            double output = double.Parse(number)/100;
            output = Math.Round(output, 2);

            return output;
        }

    }
}
