﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            //The text files will be saved on desktop
            string filePath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            string unSortedTableName = "Unsorted_Table.txt";
            string sortedTableName = "Sorted_Table.txt";

            SaveTable(GenerateTable(), filePath, unSortedTableName);
            int[,] table = LoadTable(filePath, unSortedTableName);
            int[,] sortedTable = SortTable(table);
            SaveTable(sortedTable, filePath, sortedTableName);
        }

        private static void SaveTable(int[,] table, string filePath, string fileName)
        {
            string output = "";

            for (int i = 0; i < table.GetLength(0); i++)
            {
                for (int j = 0; j < table.GetLength(1); j++)
                {
                    output += table[i, j].ToString() + ",";
                }
                output += "\n";
            }

            using (StreamWriter outputFile = new StreamWriter(Path.Combine(filePath, fileName)))
            {
                outputFile.Write(output);
            }
        }

        public static int[,] GenerateTable()
        {
            Console.Write("Enter the number of rows >");
            int rows = int.Parse(Console.ReadLine());

            Console.Write("Enter the number of columns >");
            int columns = int.Parse(Console.ReadLine());

            int[,] table = new int[rows, columns];


            Console.WriteLine("Generating Table");

            Random rnd = new Random();
            for (int i = 0; i < table.GetLength(0); i++)
            {
                for (int j = 0; j < table.GetLength(1); j++)
                {
                    table[i,j] = rnd.Next(0, 100);
                    Console.Write(table[i,j]);
                }
                Console.WriteLine();
            }
            return table;
        }

        public static int[,] LoadTable(string filePath, string fileName)
        {
            Console.WriteLine("Loading Table");
            Console.WriteLine();
            string fullPath = filePath + "\\" + fileName;
            string[] lines = File.ReadAllLines(fullPath);

            int rows = lines.Length;
            int columns = lines[0].Count(c => c == ',');

            int[,] table = new int[rows, columns];
            string currentNumber = "";
            int currentCol = 0;

            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < lines[i].Length; j++)
                {
                    if (lines[i][j] != ',')
                    {
                        currentNumber += lines[i][j].ToString();
                    }
                    else
                    {
                        table[i, currentCol] = int.Parse(currentNumber);
                        currentNumber = "";
                        currentCol++;
                    }
                }
                currentCol = 0;
                Console.WriteLine();
            }
            return table;
        }

        public static double[] CountAverageOfColumn(int[,] table)
        {
            Console.WriteLine("Counting Average Of Columns");
            int numOfRows = table.GetLength(0);
            int numOfCols = table.GetLength(1);
            double[] avgOfElemsInCols = new double[numOfCols];


            for (int j = 0; j < numOfCols; j++)
            {
                double currentSum = 0;
                for (int i = 0; i < numOfRows; i++)
                {
                    currentSum += table[i, j];
                }
                avgOfElemsInCols[j] = currentSum / numOfRows;
            }

            return avgOfElemsInCols;
        }

        public static int[,] SortTable(int[,] table)
        {
            double[] avgCols = CountAverageOfColumn(table);
            Console.WriteLine("Sorting table");
            int rows = table.GetLength(0);
            int cols = table.GetLength(1);
            int[,] sortedTable = new int[rows, cols];

            Dictionary<int, double> avgColsDict = new Dictionary<int, double>();
            for (int i = 0; i < avgCols.Length; i++)
            {
                avgColsDict.Add(i, avgCols[i]);
            }
            //var sortedAvgColsDict = from entry in avgColsDict orderby entry.Value ascending select entry;
            var sortedAvgColsDict = avgColsDict.OrderBy(entry => entry.Value);
            var sortedDict = sortedAvgColsDict.ToDictionary(pair => pair.Key, pair => pair.Value);
            List<int> sortedColsNumbers = new List<int>(sortedDict.Keys);

            for (int j = 0; j < cols; j++)
            {
                for (int i = 0; i < rows; i++)
                {
                    sortedTable[i, j] = table[i, sortedColsNumbers[j]];
                    Console.Write(sortedTable[i, j]);
                }
                Console.WriteLine();
            }

            return sortedTable;

        }
    }
}
