﻿namespace TodoAppAPI.Context
{
    using Microsoft.EntityFrameworkCore;
    using TodoAppAPI.Models;

    public class TodoAppContext : DbContext
    {
        public TodoAppContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<User> Users { get; set; }
        public DbSet<TodoTask> Tasks { get; set; }
    }
}
