﻿using Microsoft.AspNetCore.Mvc;
using TodoAppAPI.Context;
using TodoAppAPI.Models;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace TodoAppAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly TodoAppContext _todoAppContext;

        public UserController(TodoAppContext todoAppContext)
        {
            _todoAppContext = todoAppContext;
        }

        // GET: api/<UserController>
        [HttpGet]
        public IEnumerable<User> Get()
        {
            return _todoAppContext.Users;
        }

        // GET api/<UserController>/5
        [HttpGet("{id}")]
        public User Get(int id)
        {
            return _todoAppContext.Users.FirstOrDefault(x => x.UserId == id);
        }

        // POST api/<UserController>
        [HttpPost]
        public void Post([FromBody] User value)
        {
            _todoAppContext.Users.Add(value);
            _todoAppContext.SaveChanges();
        }

        // PUT api/<UserController>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] User value)
        {
            var user = _todoAppContext.Users.FirstOrDefault(x => x.UserId == id);
            if (user != null)
            {
                _todoAppContext.Entry<User>(user).CurrentValues.SetValues(value);
                _todoAppContext.SaveChanges();
            }
        }

        // DELETE api/<UserController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            var user = _todoAppContext.Users.FirstOrDefault(x => x.UserId == id);
            if (user != null)
            {
                _todoAppContext.Users.Remove(user);
                _todoAppContext.SaveChanges();
            }
        }
    }
}
