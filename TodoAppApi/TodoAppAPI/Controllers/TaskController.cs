﻿using Microsoft.AspNetCore.Mvc;
using TodoAppAPI.Context;
using TodoAppAPI.Models;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace TodoAppAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TaskController : ControllerBase
    {
        private readonly TodoAppContext _todoAppContext;

        public TaskController(TodoAppContext todoAppContext)
        {
            _todoAppContext = todoAppContext;
        }

        /// <summary>
        /// Get all task user's tasks
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpGet("user/{userId}")]
        public IEnumerable<TodoTask> GetUserTasks(int userId)
        {
            return _todoAppContext.Tasks.Where(x => x.User.UserId == userId);
        }

        /// <summary>
        /// Get one task
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public TodoTask Get(int id)
        {
            return _todoAppContext.Tasks.FirstOrDefault(x => x.TaskId == id);
        }

        // POST api/<TaskController>
        [HttpPost]
        public void Post([FromBody] TodoTask value)
        {
            _todoAppContext.Tasks.Add(value);
            _todoAppContext.SaveChanges();
        }

        // PUT api/<TaskController>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] TodoTask value)
        {
            var task = _todoAppContext.Tasks.FirstOrDefault(x => x.TaskId == id);
            if (task != null)
            {
                _todoAppContext.Entry<TodoTask>(task).CurrentValues.SetValues(value);
                _todoAppContext.SaveChanges();
            }
        }

        // DELETE api/<TaskController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            var task = _todoAppContext.Tasks.FirstOrDefault(x => x.TaskId != id);
            if (task != null)
            {
                _todoAppContext.Tasks.Remove(task);
                _todoAppContext.SaveChanges();
            }
        }
    }
}
