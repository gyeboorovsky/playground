from pynput.mouse import Button
import time

def counting_down():
    print("5")
    time.sleep(1)
    print("4")
    time.sleep(1)
    print("3")
    time.sleep(1)
    print("2")
    time.sleep(1)
    print("1")
    time.sleep(1)
    print("START")

def lvl_up_clicking(mouse):
    print("Upgrade Clicking")
    mouse.position = (1150, 750)
    time.sleep(0.1)
    mouse.press(Button.left)
    mouse.release(Button.left)
    time.sleep(0.5)


def lvl_up_strongest(mouse, ammount): #ammount 1/3 to 9
    print("START UPGRADING HEROES")
    mouse.position = (1150, 225)
    mouse.scroll(0, 2)
    time.sleep(1)
    counter = 0
    for i in range(ammount):
        print("Upgrading hero {0}.".format(i + 1))
        mouse.position = (1150, 225 + 90 * counter)
        for j in range(5):
            time.sleep(0.1)
            mouse.press(Button.left)
            mouse.release(Button.left)
        counter += 1