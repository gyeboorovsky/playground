from pynput.mouse import Button
from functions import counting_down
import time

def go_to_page_1(mouse):
    print("Goint to page 1")
    mouse.position = (720, 1030)
    time.sleep(0.1)
    mouse.press(Button.left)
    mouse.release(Button.left)
    time.sleep(0.5)

def go_to_page_2(mouse):
    print("Going to page 2")
    mouse.position = (820, 1030)
    time.sleep(0.1)
    mouse.press(Button.left)
    mouse.release(Button.left)
    time.sleep(0.5)

def maximize_page(mouse):
    print("Maximize page")
    mouse.position = (1130, 600)
    time.sleep(0.1)
    mouse.press(Button.left)
    mouse.release(Button.left)
    time.sleep(2)
    mouse.press(Button.left)
    mouse.release(Button.left)
    time.sleep(0.5)

def minimize_page(mouse):
    print("Minimize page")
    mouse.position = (1130, 60)
    time.sleep(0.1)
    mouse.press(Button.left)
    mouse.release(Button.left)
    time.sleep(2)
    mouse.press(Button.left)
    mouse.release(Button.left)
    time.sleep(0.5)

def position_to_clicking(mouse):
    print("Set mouse position to clicking")
    mouse.position = (1150, 500)

def prestige(mouse): #from first page, medium window, scrolled up
    print("IT'S TIME TO PRESTIGE!!!!")
    mouse.position = (1150, 870)

    mouse.press(Button.left)
    mouse.release(Button.left)
    time.sleep(1)

    mouse.position = (950, 960)
    mouse.press(Button.left)
    mouse.release(Button.left)
    time.sleep(1)

    mouse.position = (1060, 800)
    mouse.press(Button.left)
    mouse.release(Button.left)

    counting_down()