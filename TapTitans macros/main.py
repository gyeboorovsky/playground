from pynput.mouse import Button, Controller
import functions
import moves
import autoclicking

#CFG
begining_upgrading = 0 #if != 0 switch to secound maximize page

mouse = Controller()
autoclicking_factor = 0

#START
functions.counting_down()


def full_cycle():
    for i in range(begining_upgrading):
        print("{0} MORE UPGRADES".format(begining_upgrading - i))
        functions.lvl_up_strongest(mouse, 9)
    if begining_upgrading:

        moves.minimize_page(mouse)

        moves.go_to_page_1(mouse)

    while True:

        functions.lvl_up_clicking(mouse)

        moves.go_to_page_2(mouse)

        moves.maximize_page(mouse)

        functions.lvl_up_strongest(mouse, 9)

        moves.minimize_page(mouse)

        moves.go_to_page_1(mouse)

        moves.position_to_clicking(mouse)

        functions.counting_down()


        if autoclicking_factor < 30:
            autoclicking.clicking_for_1_min(mouse)
            autoclicking_factor += 1
        # elif autoclicking_factor < 30:
        #     autoclicking.clicking_for_10_min(mouse)
        #     autoclicking_factor += 1
        # elif autoclicking_factor < 12:
        #     autoclicking.clicking_for_30_min(mouse)
        #     autoclicking_factor += 1
        else:
            autoclicking_factor = 0
            moves.prestige(mouse)
            moves.position_to_clicking(mouse)

def upgrade_4_best_cycle():
    autoclicking_factor = 0
    cycles = 1000
    while True:
        while autoclicking_factor < cycles:
            print("{0} of {1}".format(autoclicking_factor + 1, cycles))
            functions.lvl_up_strongest(mouse, 4)
            autoclicking_factor += 1

        moves.minimize_page(mouse)

        moves.go_to_page_1(mouse)

        moves.prestige(mouse)

        moves.go_to_page_2(mouse)

        moves.maximize_page(mouse)

        autoclicking_factor = 0

upgrade_4_best_cycle()
