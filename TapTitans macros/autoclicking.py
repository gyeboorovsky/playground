from pynput.mouse import Button
from moves import prestige
import time

def progress(progress, period):
    percentage = (progress / period) * 100
    percentage = float("{0:.2f}".format(percentage))
    if percentage % 1 == 0:
        print("Period {0}, progress: {1:.0f}%".format(period, percentage))

def clicking_for_1_min(mouse):
    print("START AUTOCLICKING")
    counter = 0
    period = 60
    while counter < period:
        progress(counter, period)
        mouse.click(Button.left)
        counter += 0.01
        time.sleep(0.01)

def clicking_for_10_min(mouse):
    print("START AUTOCLICKING")
    counter = 0
    period = 600
    while counter < period:
        progress(counter, period)
        mouse.click(Button.left)
        counter += 0.01
        time.sleep(0.01)

def clicking_for_30_min(mouse):
    print("START AUTOCLICKING")
    counter = 0
    period = 1800
    while counter < period:
        mouse.click(Button.left)
        counter += 0.01
        time.sleep(0.01)

# def prestige(mouse):
    # mouse.position = (1150, 870)
    #
    # mouse.press(Button.left)
    # mouse.release(Button.left)
    # time.sleep(1)
    #
    # mouse.position = (950, 920)
    # mouse.press(Button.left)
    # mouse.release(Button.left)
    # time.sleep(1)
    #
    # mouse.position = (1060, 760)
    # mouse.press(Button.left)
    # mouse.release(Button.left)
    # time.sleep(1)

# mouse = Controller()
# mouse.position = (1150, 225)
# mouse.scroll(0, 2)
