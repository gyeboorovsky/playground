def compute_sum(n):
    #return sum(int(c) for i in range(1, n+1) for c in str(i))
    sum = 0
    for i in range(1, n+1):
        for c in str(i):
            sum += int(c)
    return sum
print(compute_sum(10))